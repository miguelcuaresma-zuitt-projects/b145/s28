// create a standard server using Node.
// 1. identify the ingredients/components needed in order to perform the task.

// http -> will allow Node JS to establish a connection in order to tranfer data using the Hyper Text Transfer Protocol

// require() -> this will allow us to acquire the resources needed to perform the task

let http = require("http");

//2. create a server using methods that we will take from the http resources

// the http module contains the createServer() method will allow us to create a standard server setup.
//3. identify the procedure/task that the server will perform once the connection has been established.

http.createServer((request, response) => {
	response.end("Welcome to the server, Batch 145!");
}).listen(3000);

//4. select an address/location where the connection will be established/hosted.

//5. create a response in the terminal to make sure if the server is running succesfully.
console.log("Server is running successfully");

//careful when executing this command: npx kill-port <address>

//6. Initialize a node package manager in our project.

//7. Install your first package/dependency from the NPM that will allow you to make certain tasks a lot more easier

//8. Create a remote repository to backup the project.
	//NOTE: the node_modules should not be included in the files structure that will be saved online

	//1. its going to take a longer time to stage/push your projects online
	//2. the node_modules will cause error upon deployment